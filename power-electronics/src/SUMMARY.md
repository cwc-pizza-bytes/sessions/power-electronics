# Summary

[Welcome to Power Electronics](welcome.md)

## Body

- [Theory of switches](theory/Theory.md)
  - [Diodes](theory/diodes.md)
  - [Transistors](theory/transistor.md)
- [Simple Switch](Session/simpleswitch.md)
- [FET Ron](Session/Ron.md)
- [Darlington Pair](Session/darlingtonpair.md)
- [H-Bridge inverter](Session/h_bridge.md)
- [DCC Circuit](Session/DCCcircuit.md)
- [Receiver](Session/Receiver.md)
  - [Rectification](Session/Rectification.md)
  - [Regulation](Session/Regulation.md)
  - [Isolation](Session/Isolation.md)