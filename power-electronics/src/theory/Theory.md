# Theory of switches

## In this first section, we will cover a brief overview of some types of switches and scratch on how they work

Electronic switches are fundamental components in electronic circuits that control the flow of electrical current. They are used to turn devices or circuits on and off, redirect current paths, or regulate the flow of electricity based on specific conditions.
