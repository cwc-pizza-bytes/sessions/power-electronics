# Isolation
Now that we have power sorted, we need to filter out the communications signal with the help of our good friend the diode, but not the same type. For this we are going to use an optoisolator which contains a light emitting diode (LED) and a photosensitive transistor. When the LED is powered, it illuminates and switches on the transistor. This ensures that the sensitive electronics are isolated from the higher voltage of the rails and any random power spikes that may occur.

<img src="../assets/PC817.jpg" alt="drawing" width="400"/>

As we know a diode only works on one direction, which means the IR LED will only illuminate and thus turn on the transistor, when the rails are supplying positive current. We can then pass current through the transistor and detect when it is on or off through the Raspberry Pi Pico

// image of circuit

// image of waveforms

The next tasks would be to figure out how to measure the length of the pulses to decode the data.