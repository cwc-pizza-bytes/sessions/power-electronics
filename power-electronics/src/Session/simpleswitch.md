# Simple Switch

The first step is creating a simple switch from our Mosfet.

The circuit below shows a simple circuit where a pico pin can be used to drive a MOSFET gate to turn on the circuit and power the LED. The pulldown resistor is used so that the voltage is pulled down to ground when the pin is disconnected or the pico is not providing a signal.

Note:- Pico has configurable pulldown pins. "button = Pin(14, Pin.IN, Pin.PULL_DOWN)"

![simpleCircuit](../assets/picoswitch.png)

So Lets give it ago and see if you can get a LED to blink!

It's best to build the circuit before you connect the Battery.

Ready made code for this is provided: [PicoFile](../files/power.uf2) (This should make pins 15 & 16 alternate on for 1 second)

<img src="../assets/SS_On.JPG" alt="drawing" width="300"/> <img src="../assets/SS_Off.JPG" alt="drawing" width="300"/>


### Typical layout of PNP and NPN

The example shown above is an NPN. For a PNP you would put the load (LED) below in the switch in the circuit. A PNP type FET requires a negative Voltage as referenced from the drain which is attached to the Positive side of the Power.

<img src="../assets/typicalfet.png" alt="TypicalFetLayout" width="400"/>
