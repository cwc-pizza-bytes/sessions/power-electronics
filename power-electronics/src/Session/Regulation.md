# Regulation

## Concept
As the name suggests we are going to regulate the supply, much like a gas or water regulator will enable a steady flow, we can also do this in electronics. 

The regulator will take in a dirty supply voltage and output a clean regulated voltage which is much more suitable for digital electronics. 

## Regulators

We are going to use a simple LM7805 linear regulator for this as they are cheap and require very few components. 

<img src="../assets/LM7805.png" alt="drawing" width="400"/>

There are also switch mode regulators which add the benefits of being more efficient, they don't get as hot, they are capable of outputting a higher voltage than the input voltage and they can produce a negative voltage. They are however more expensive and require a lot more components. These would be more ideal in circuits that deal with analogue signals and run on batteries. But for our application, a linear regulator is better suited.

## Circuit

So we simply need 1x LM7805 and 2x 100nF capacitors. The capacitors help with the smoothing process and remove any unwanted noise from the supply.

// image of the circuit

That’s it, let's check out the output.

// image of the oscilloscope output