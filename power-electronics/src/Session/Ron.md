# FET Rds(on)

The only problem with the simple switch is an internal resistance in the MOSFET due to it being driven with a low input voltage, you can think of this like a valve or tap only being open half way.

In high power applications, we want to minimise the resistance to any switching devices as this causes a waste of heat and a restriction of power.

The Gate-source threshold voltage, or the voltage that the MOSFET will start to turn on at is as low as 2-4V for this device. however it will have a much higher resistance internally than if we were to drive the gate pin with closer to 10V.

This is shown in the graph below, the Drain Current is higher for the same voltage across the MOSFET Vds when the voltage Vgs is higher.

![Rds](../assets/Rds(on)Arrows.png)

Here is the [datasheet](https://www.vishay.com/docs/91021/irf540.pdf) if you want to see all the techy details.

So how do we use our Pico Pin input voltage of 3~5V to drive the gate at a higher voltage?
