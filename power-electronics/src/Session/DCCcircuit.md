# DCC Circuit

For our DCC system, instead of the Motor, we would connect one side of our circuit to one side of the tracks, and the other side to the other. And the modulation of the switching creates our encoded signal to send commands to our Trains. The trains will always receive the same voltage, it will just flip from positive to negative.

![dcc](../assets/DCCcircuit.png)

And we can use the same Inverter with a motor in the middle for the train to control the direction the motor spins. And using the PWM technique in the previous session that will help control the speed of the motor.
