# Darlington Pair & Sziklai Pair

Instead of using the low voltage from the Pico to drive the FET gate, we can use a pair of MOSFETs to pull the gate to ground or the higher voltage from the voltage source. In our case that will give us 9 volts Vgs instead of ~3.5-5v. This will mean less power and heat wasted in the switches.

We will use the BS170 NPN MOSFET (although ours are cheap amazon ones and the Drain and Source pins are swapped arround!) for the one conncected to the Pico, and the Nchannel MOSFET IRF540 for the one connceted to the load (LED/Resisistor).

## Circuit for Darlington / Sziklai Pair

Notice how the first MOSFET is pulling the Gate of the second down to ground (0v) when its on, else when the first MOSFET is off the gate will be pulled up to high (9v). this will cause the second MOSFET to be inverted to the first.

Give it a try on your breadboards.

<img src="../assets/SZCircuit.png" alt="drawing" width="600"/>