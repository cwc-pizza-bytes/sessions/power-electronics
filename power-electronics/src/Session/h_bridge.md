# Building our H-Bridge

Our DCC system will need to see a +15/-15v AC at its power source, so we need to try and create an alternating voltage source out of our DC batteries. To do this we will create an inverter out of what we have just been learning. The Inverter we will be making is a H Bridge Inverter.

In order to make our H Bridge Inverter we need to add a few more parts to our circuit.

First, a PNP MOSFET (IRF9540).

Make sure the circuit is not powered up while we change the circuit around incase something gets hot or goes bang!

We will first remove the load resistor and LED. (or which ever device you used as a load)

Next place the PNP MOSFET where the load was before, connecting the NPN through it to the +ve power rail. Finally connect the Gates of the P and N mosfet together. This should take your circuit on the right to the circuit on the left.

Note: You do not need to test this. It should not do anything anyway.

<img src="../assets/SZCircuit.png" alt="SZcircuit" width="300"/>

<img src="../assets/PandN.png" alt="darlintonpair" width="300"/>

Next we need to duplicate our circuit in a mirror image of itself, and connect our load between the mid point of each side of MOSFETS.

<img src="../assets/HBridge.png" alt="Hbridge" width="600"/>

<img src="../assets/HBcircuit.png" alt="Hbcircuit" width="600"/>

The 2 pins of the inputs must not be connected to the same pin. On your Pico code provided the pins GP15 and GP16 (two pins opposite each other, and opposite the USB connector) will alternate high and low.

## How it works:

The way this works is providing a path across the load for current to flow so that when one side of the circuit it turned on the current will flow one way:

![flowforward](../assets/HBridgeLR.png)

And when the other side is on, the current will flow the other way:

![flowforward](../assets/HBrightRL.png)

As you can see the current flows through paths of the H, which is how it gets its name.

This causes an alternating current (AC) and voltage to be seen by the load. In the case of a DC motor that will make it flow forwards and then backwards.

Here's a link to a little simulation of this in action:
[Tinkercad-H-bridge](https://www.tinkercad.com/things/jvogDkpM5uz)

Another breadboard Circuit:

<img src="../assets/TransmitterCircuit.jpg" alt="Transmitter Circuit" width="900"/>