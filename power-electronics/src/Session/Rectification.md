# Rectification
We need to power our train and provide communications to tell it which way to go and how fast etc. Currently, the power and communication signal are mixed together in the form of a differential signal. 

<img src="../assets/WAVE1.JPG" alt="drawing" width="600"/>

So we need to separate the power from the communications, let's start with power. 
In this session we will look at the concept of Rectification which enables us to convert an alternating current supply into a direct current supply.


## Concept

Imagine waves lapping up on a beach and you've dug a hole and you want the hole to be filled by the waves. So you make a river connecting the waves to the hole. 

The wave comes in and flows up the river and just about reaches the hole and then the wave goes back out. Annoyingly not much water went in the hole, so you create a one way valve with a stick and a spade. Now the wave comes up the river but as it starts to flow back out, it is blocked by the one way valve and some water is stored in the river, then the next wave comes and pushes the water stored in the river into the hole and the cycle repeats, Very slowly filling the hole.

## Diodes Recap
<img src="../assets/diode.png" alt="drawing" width="400"/>

As previous seen, diodes allow the flow of current only in one direction. 
When a diode is used on an alternating current (AC) supply, it can prevent the current from flowing in the reverse direction. 

They effectively work like a one way valve.

## Rectifier

We can create a simple rectifier circuit just like the beach example above, by using a diode in series with the supply.

<img src="../assets/SingleDiodeRectifier.png" alt="drawing" width="400"/>

Let's measure the input vs the output 

<iframe src="https://www.multisim.com/content/iYBC2bXg4MRwQcxRGcRdai/diode-demo/open" width="800" height="600"></iframe>

As we can see the positive part of the sine wave is able to pass through but the negative part is being blocked.

Unfortunately, this setup means that we are only getting half of the power that is being supplied by the signal.


## Full Bridge Rectifier

If we use multiple diodes in a star configuration, we can then convert the power delivered by the negative part of the sine wave back into a possitive voltage. 

<img src="../assets/FullBridgeRectifier.png" alt="drawing" width="400"/>

Lets test the input verses the output

<iframe width="800" height="600" src="https://www.multisim.com/content/dRDfBNH4vGH8d75tqFC8qU/full-bridge-rectifier/open"></iframe>

So as we can see here, the negative part of the sine wave supply has now been converted into a positive waveform, so now we have access to the full amount of power from the AC supply.

But this is currently not a constant current as it the power is in waves, so we need to smooth this out using a capacitor. This is basically the same concept as the hole you dug earlier. 

## Smoothing

<img src="../assets/FBRSmoothed.png" alt="drawing" width="400"/>

Now, every time a wave comes in, the capacitor (or the hole) gets charged up and can be used to power the rest of the circuit. 

<iframe width="800" height="600" src="https://www.multisim.com/content/bHWaXNkPZ9cVHAyYG42h69/full-bridge-rectifier-with-smoothing/open"></iframe>

This output would be fine for some very basic circuits like powering a DC light, or a motor in one direction and at a constant speed. But for some more advanced circuits like using microcontrollers and logic devices, we need a more stable power supply. So let's move onto regulation.